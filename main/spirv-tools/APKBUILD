# Contributor: Leo <thinkabit.ukim@gmail.com>
# Maintainer: Simon Zeni <simon@bl4ckb0ne.ca>
pkgname=spirv-tools
_pkgname=SPIRV-Tools
pkgver=1.3.211.0
_pkgver=sdk-1.3.211.0
pkgrel=0
pkgdesc="API and commands for processing SPIR-V modules"
url="https://github.com/KhronosGroup/SPIRV-Tools"
arch="all"
license="Apache-2.0"
depends_dev="spirv-headers $pkgname=$pkgver-r$pkgrel"
makedepends="$depends_dev cmake samurai python3"
subpackages="$pkgname-dev $pkgname-dbg"
source="$pkgname-$_pkgver.tar.gz::https://github.com/KhronosGroup/$_pkgname/archive/$_pkgver.tar.gz"
builddir="$srcdir/$_pkgname-$_pkgver"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi

	cmake -B build -G Ninja \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DCMAKE_BUILD_TYPE=MinSizeRel \
		-DBUILD_SHARED_LIBS=ON \
		-DSPIRV_WERROR=OFF \
		-DSPIRV-Headers_SOURCE_DIR=/usr \
		-DSPIRV_TOOLS_BUILD_STATIC=OFF \
		$CMAKE_CROSSOPTS
	cmake --build build
}

check() {
	# Not all test are enabled, because they rely on googltest source folder
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
df7de69779baa7ea2269cb4c9813a7d8026763f7d4f7408ea5c160ca7bd670e46e75dcbf9d99ca5f1619f37f504e695d151ede5d5cd0ef675a8898bb297c5842  spirv-tools-sdk-1.3.211.0.tar.gz
"
