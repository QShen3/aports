# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kcolorchooser
pkgver=22.04.1
pkgrel=0
# armhf blocked by extra-cmake-modules
# s390x and riscv64 blocked by polkit -> kxmlgui
arch="all !armhf !s390x !riscv64"
url="https://kde.org/applications/graphics/org.kde.kcolorchooser"
pkgdesc="A color palette tool, used to mix colors and create custom color palettes"
license="MIT"
makedepends="
	extra-cmake-modules
	ki18n-dev
	kxmlgui-dev
	qt5-qtbase-dev
	samurai
	"
source="https://download.kde.org/stable/release-service/$pkgver/src/kcolorchooser-$pkgver.tar.xz"
subpackages="$pkgname-lang"
options="!check" # No tests

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
7ed86550e65546e02d81755ffaebec6c0533880968921aa678294e08b3ef4d94664281f5fcf3baca5591355c2bd7fe824850c86f525c8121d5d236fecb27a8d8  kcolorchooser-22.04.1.tar.xz
"
