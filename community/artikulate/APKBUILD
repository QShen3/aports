# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=artikulate
pkgver=22.04.1
pkgrel=0
# armhf blocked by qt5-qtdeclarative
# s390x and riscv64 blocked by polkit -> knewstuff
arch="all !armhf !s390x !riscv64"
url="https://edu.kde.org/artikulate"
pkgdesc="Improve your pronunciation by listening to native speakers"
license="(GPL-2.0-only OR GPL-3.0-only) AND GFDL-1.2-only"
depends="kirigami2"
makedepends="
	extra-cmake-modules
	karchive-dev
	kconfig-dev
	kcrash-dev
	kdoctools-dev
	ki18n-dev
	kirigami2-dev
	knewstuff-dev
	kxmlgui-dev
	qt5-qtbase-dev
	qt5-qtdeclarative-dev
	qt5-qtmultimedia-dev
	qt5-qtxmlpatterns-dev
	samurai
	"
source="https://download.kde.org/stable/release-service/$pkgver/src/artikulate-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=MinSizeRel \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
96a66811d31a083aedd0163e984c8c34865ca61a128f5f024c48c154e46dfe067dd2df89bc237571710ab20114e032cbb1ea7d309063c00f4a62b6f4bd01c592  artikulate-22.04.1.tar.xz
"
