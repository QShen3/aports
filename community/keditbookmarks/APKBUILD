# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=keditbookmarks
pkgver=22.04.1
pkgrel=0
# armhf blocked by extra-cmake-modules
# s390x blocked by polkit -> kio
arch="all !armhf !s390x !riscv64"
url="https://www.kde.org/"
pkgdesc="Bookmark Organizer and Editor"
license="GPL-2.0-only AND GFDL-1.2-only"
makedepends="
	extra-cmake-modules
	kbookmarks-dev
	kdoctools-dev
	ki18n-dev
	kiconthemes-dev
	kio-dev
	kparts-dev
	kwindowsystem-dev
	qt5-qtbase-dev
	samurai
	"
checkdepends="xvfb-run"
source="https://download.kde.org/stable/release-service/$pkgver/src/keditbookmarks-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE xvfb-run ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
b1977047693f7cdad47906a03c2b5ce8ac8f1df599de8f80979a66dcd1825b51b10053dcbd8dc65670dd69cfe89c6ed98918027561e16ded720b870415a2f618  keditbookmarks-22.04.1.tar.xz
"
