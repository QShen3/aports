# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=partitionmanager
pkgver=22.04.1
pkgrel=0
# armhf blocked by extra-cmake-modules
# s390x and riscv64 blocked by polkit -> kio
arch="all !armhf !s390x !riscv64"
url="https://kde.org/applications/system/org.kde.partitionmanager"
pkgdesc="A KDE utility that allows you to manage disks, partitions, and file systems"
license="GPL-3.0-or-later"
makedepends="
	extra-cmake-modules
	kconfig-dev
	kconfigwidgets-dev
	kcoreaddons-dev
	kcrash-dev
	kdbusaddons-dev
	kdoctools-dev
	ki18n-dev
	kiconthemes-dev
	kio-dev
	kjobwidgets-dev
	kpmcore-dev
	kwidgetsaddons-dev
	kxmlgui-dev
	qt5-qtbase-dev
	samurai
	"
source="https://download.kde.org/stable/release-service/$pkgver/src/partitionmanager-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
0fe767d624f202a7448904071961c2b71aa6922196fb51ea5adaa3a12a81db4b1981a36b66f6f689fff69848b6a6d1af7d7a9d5c82f684dafc587ece89755736  partitionmanager-22.04.1.tar.xz
"
