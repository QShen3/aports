# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kmime
pkgver=22.04.1
pkgrel=0
pkgdesc="Library for handling mail messages and newsgroup articles"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://community.kde.org/KDE_PIM"
license="LGPL-2.0-or-later"
depends_dev="
	kcodecs-dev
	ki18n-dev
	qt5-qtbase-dev
	"
makedepends="$depends_dev
	extra-cmake-modules
	samurai
	"
source="https://download.kde.org/stable/release-service/$pkgver/src/kmime-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build

	# kmime-headertest and kmime-messagetest are broken
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest -E "kmime-(header|message)test"
}

package() {
	DESTDIR="$pkgdir" cmake --build build --target install
}

sha512sums="
af26ebbe83f043373c5c5416e1bd0150f1e6d43469ed9dd5ac9081e34a24668da9554a0129f1bdf52e007030ab9adf504fe113025e6ecf29b801e62a71593327  kmime-22.04.1.tar.xz
"
