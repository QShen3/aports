# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kontactinterface
pkgver=22.04.1
pkgrel=0
# armhf blocked by extra-cmake-modules
# s390x and riscv64 blocked by polkit -> kparts
arch="all !armhf !s390x !riscv64"
url="https://kontact.kde.org/"
pkgdesc="Kontact Plugin Interface Library"
license="LGPL-2.0-only OR LGPL-3.0-only"
depends_dev="
	kcoreaddons-dev
	ki18n-dev
	kiconthemes-dev
	kparts-dev
	kwindowsystem-dev
	kxmlgui-dev
	samurai
	"
makedepends="$depends_dev extra-cmake-modules"
source="https://download.kde.org/stable/release-service/$pkgver/src/kontactinterface-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
0aee45236fc699a9b6d8b21eea610dd5899098e73cb1d75a7bd6df9cd81d77c171d75d7f9d519eeb91c46011f952f46f41da16e7cdc41d6082ed9e25e920e511  kontactinterface-22.04.1.tar.xz
"
