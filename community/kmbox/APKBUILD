# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kmbox
pkgver=22.04.1
pkgrel=0
pkgdesc="Library for accessing mail storages in MBox format"
# armhf blocked by extra-cmake-modules
# s390x blocked by kmime
arch="all !armhf !s390x"
url="https://kontact.kde.org/"
license="LGPL-2.0-or-later"
depends_dev="kmime-dev"
makedepends="$depends_dev extra-cmake-modules samurai"
source="https://download.kde.org/stable/release-service/$pkgver/src/kmbox-$pkgver.tar.xz"
subpackages="$pkgname-dev"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --build build --target install
}

sha512sums="
882a901e9435a31e7dff9df386990e8aa34d34b14f00c546c80132c218a97a0485797a9e7d9f4deef31e3edff81504af244d1883341e2a11bffa4b5c7fee74d0  kmbox-22.04.1.tar.xz
"
