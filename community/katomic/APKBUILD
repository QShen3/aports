# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=katomic
pkgver=22.04.1
pkgrel=0
# armhf blocked by extra-cmake-modules
# s390x and riscv64 blocked by polkit -> kxmlgui
arch="all !armhf !s390x !riscv64"
url="https://kde.org/applications/games/org.kde.katomic"
pkgdesc="A fun and educational game built around molecular geometry"
license="GPL-2.0-or-later AND GFDL-1.2-only"
makedepends="
	extra-cmake-modules
	qt5-qtbase-dev
	kcoreaddons-dev
	kconfig-dev
	kcrash-dev
	kwidgetsaddons-dev
	ki18n-dev
	kxmlgui-dev
	knewstuff-dev
	kdoctools-dev
	kdbusaddons-dev
	libkdegames-dev
	samurai
	"
source="https://download.kde.org/stable/release-service/$pkgver/src/katomic-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"
options="!check" # No tests

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
cfb6c00dfe5c5c05b9044ed9c8ce84f8f9b571b2e86649c3084e03eb624b39d6bf63bb069a3aff78e6e97ced7d44d68a675052483ffc2ea7d4a6ab0a1c72edc8  katomic-22.04.1.tar.xz
"
