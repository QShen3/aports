# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=knotes
pkgver=22.04.1
pkgrel=0
# armhf blocked by extra-cmake-modules
# ppc64le and s390x blocked by kdepim-runtime
# riscv64 disabled due to missing rust in recursive dependency
arch="all !armhf !ppc64le !s390x !riscv64"
url="https://kontact.kde.org/"
pkgdesc="Popup notes"
license="GPL-2.0-or-later AND LGPL-2.0-or-later AND GFDL-1.2-only"
depends="kdepim-runtime"
makedepends="extra-cmake-modules qt5-qtbase-dev qt5-qtx11extras-dev grantlee-dev kcompletion-dev kconfig-dev kconfigwidgets-dev kcoreaddons-dev kcrash-dev kdbusaddons-dev kdnssd-dev kdoctools-dev kglobalaccel-dev kiconthemes-dev kitemmodels-dev kitemviews-dev kcmutils-dev knewstuff-dev knotifications-dev knotifyconfig-dev kparts-dev ktextwidgets-dev kwidgetsaddons-dev kwindowsystem-dev kxmlgui-dev knotifications-dev akonadi-dev akonadi-notes-dev kcalutils-dev kontactinterface-dev libkdepim-dev kmime-dev pimcommon-dev kpimtextedit-dev grantleetheme-dev akonadi-search-dev libxslt-dev samurai"
checkdepends="xvfb-run"
source="https://download.kde.org/stable/release-service/$pkgver/src/knotes-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build

	# knotesgrantleeprinttest is broken
	CTEST_OUTPUT_ON_FAILURE=TRUE xvfb-run ctest -E "knotesgrantleeprinttest"
}

package() {
	DESTDIR="$pkgdir" cmake --build build --target install
}

sha512sums="
3747d3127dc6541791138cf75a9e6be3cab6191b497d2e3b14fc655725db511d040e311a53b9e7a8b4b1ae7c80c8f74157a1b22998c8ac3500c2a12a42db14ff  knotes-22.04.1.tar.xz
"
