# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kiriki
pkgver=22.04.1
pkgrel=0
pkgdesc="An addictive and fun dice game"
# armhf blocked by extra-cmake-modules
# s390x and riscv64 blocked by polkit -> kxmlgui
arch="all !armhf !s390x !riscv64"
url="https://kde.org/applications/games/kiriki/"
license="GPL-2.0-or-later AND GFDL-1.2-only"
makedepends="
	extra-cmake-modules
	kconfig-dev
	kconfigwidgets-dev
	kcoreaddons-dev
	kcrash-dev
	kdbusaddons-dev
	kdoctools-dev
	ki18n-dev
	kiconthemes-dev
	kxmlgui-dev
	libkdegames-dev
	qt5-qtbase-dev
	samurai
	"
source="https://download.kde.org/stable/release-service/$pkgver/src/kiriki-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"
options="!check" # No tests

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
be21404abb0d061a731c880d4e265612dd6f028ef2e3623e0f411ef603e6e2392789cb060f83263c53db6727a665cb2a4f6056fd6a50dd825c59915e6564a8c5  kiriki-22.04.1.tar.xz
"
