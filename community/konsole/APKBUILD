# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=konsole
pkgver=22.04.1
pkgrel=1
# armhf blocked by qt5-qtdeclarative
# s390x and riscv64 blocked by polkit -> kio
arch="all !armhf !s390x !riscv64"
url="https://kde.org/applications/system/konsole"
pkgdesc="KDE's terminal emulator"
license="GPL-2.0-only AND LGPL-2.1-or-later AND Unicode-DFS-2016"
depends="ncurses-terminfo-base"
makedepends="
	extra-cmake-modules
	kbookmarks-dev
	kcompletion-dev
	kconfig-dev
	kconfigwidgets-dev
	kcoreaddons-dev
	kcrash-dev
	kdbusaddons-dev
	kdoctools-dev
	kglobalaccel-dev
	kguiaddons-dev
	ki18n-dev
	kiconthemes-dev
	kinit-dev
	kio-dev
	knewstuff-dev
	knotifications-dev
	knotifyconfig-dev
	kparts-dev
	kpty-dev
	kservice-dev
	ktextwidgets-dev
	kwidgetsaddons-dev
	kwindowsystem-dev
	kxmlgui-dev
	qt5-qtbase-dev
	samurai
	"
checkdepends="xvfb-run"
source="https://download.kde.org/stable/release-service/$pkgver/src/konsole-$pkgver.tar.xz
	0001-fix-musl-compatibility.patch
	0002-Fix-scroll-position-jumps-regression.patch
	"
subpackages="$pkgname-doc $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DENABLE_PLUGIN_SSHMANAGER=ON
	cmake --build build
}

check() {
	cd build

	# DBusTest requires running DBus
	# HistoryTest is broken
	CTEST_OUTPUT_ON_FAILURE=TRUE xvfb-run ctest -E "(DBus|History)Test"
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
34781ebea899f8687a4b71e19ab82d062139389771475246738b4e3e93776c6bb183d29fd6f62988f519e92706d65a4d194e198cfc263e18a8eb857597fcba30  konsole-22.04.1.tar.xz
6f9f5eb53629df0392abe26391a137c3bb9ced79dbf3e649d99046318d6c897e5803bc31eeb25fd042ce24c4f124a5b0fede8c90eb2b4a5682d46826e4e9c44c  0001-fix-musl-compatibility.patch
b75e4c1dc6061d0d130edb9287cd9a1cd310a0419a9448a5ac93660c13167b56d9ec2bd82e3a3800d26a9f99decb8d7bb4fd6c96acfe660ecfc29f4ac6aca52b  0002-Fix-scroll-position-jumps-regression.patch
"
